<i>Futbol Club Barcelona, FC Barcelona, Barça hiszpański wielosekcyjny klub sportowy, istniejący od chwili założenia drużyny piłkarskiej. Założony w 1899 roku przez grupę Szwajcarów, Anglików i Hiszpanów, z czasem stał się katalońską instytucją o dużym znaczeniu społecznym. Jego dewiza to Més que un club.</i>
<br />Ile dni minęło od założenia klubu:<b> <span id="licznik"></span></b><br />
<i>Na przestrzeni <img class="ui small left floated image" src="./images/oklubie2.jpg"/> ponad stu lat Barça odnosiła więcej zwycięstw w tej rywalizacji, włączając w rachunki również mecze nieoficjalne. Zawodnicy ci wygrali 105 spotkań, a przegrali 92 razy. Wynik remisowy padł w 55 meczach. Statystyka strzelonych bramek również jest korzystna dla FC Barcelony – 437 strzelonych bramek, natomiast liczba goli strzelonych dla madryckiego zespołu wynosi 410.

Jednak to Real Madryt może się poszczycić większą ilością bramek zdobytych w jednym meczu. Najbardziej <img class="ui small right floated image" src="./images/oklubie3.jpg"/>dotkliwa porażka Dumy Katalonii miała miejsce w 1935, kiedy to przegrała 1:11 (związana ona była z reżimem gen. Franco, którego ludzie weszli do szatni zawodników Barcelony po wygraniu pierwszego meczu Pucharu Króla, grożąc ich bliskim śmiercią i rodzinom). Natomiast rok później FC Barcelona na wyjeździe wygrała z Blancos 5:0. Tylko dwóm trenerom Barçy udało się co najmniej dwukrotnie pokonać Królewskich na wyjeździe – Frankowi Rijkaardowi – w sezonach 2003/2004 i 2005/2006 i Pepowi Guardioli aż trzykrotnie – w sezonach 2008/2009, 2009/2010 i 2011/2012 
 
Josepowi Guardioli jako jedynemu trenerowi w historii udało się wygrać 5 Klasyków z rzędu.<br />
Po sprzedaniu obiektu Les Corts innemu klubowi, władze FC Barcelona zdecydowały się na budowę jeszcze większego stadionu – Camp Nou. Jego budowa trwała 3 lata i kosztowała 300 mln peset. Inauguracyjne spotkanie rozegrano 24 września 1957r., a przeciwnikiem była Reprezentacja Warszawy, która przegrała 2:4.</i><br /><br />
<img class="ui large image centered" src="./images/oklubie.png"/>
<script>
var then = new Date(1899, 11, 29);
var now  = new Date;
var count = Math.round((now - then) / (1000 * 60 * 60 * 24));
$('#licznik').html(count);
</script>