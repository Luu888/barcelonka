-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 04 Lip 2019, 13:18
-- Wersja serwera: 10.1.38-MariaDB
-- Wersja PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `barcelona`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kontakt`
--

CREATE TABLE `kontakt` (
  `id_kontaktu` int(11) NOT NULL,
  `email` text COLLATE utf8_polish_ci,
  `kiedy` date NOT NULL,
  `tresc_kontaktu` text COLLATE utf8_polish_ci,
  `numer_telefonu` int(11) NOT NULL,
  `plec` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `kontakt`
--

INSERT INTO `kontakt` (`id_kontaktu`, `email`, `kiedy`, `tresc_kontaktu`, `numer_telefonu`, `plec`) VALUES
(1, 'kosek@kosek.pl', '1992-11-11', 'bo jestem dobrym adminem', 666777888, 'Mężczyzna'),
(2, 'asoskajsoidajsoi@wp.pl', '2000-11-23', 'bajsdpokasjpodkjakopdaSofpmsenoipcsfdvuspidnbajsdpokasjpodkjakopdaSofpmsenoipcsfdvuspidnbajsdpokasjpodkjakopdaSofpmsenoipcsfdvuspidnbajsdpokasjpodkjakopdaSofpmsenoipcsfdvuspidnbajsdpokasjpodkjakopdaSofpmsenoipcsfdvuspidn', 444333555, 'Mężczyzna'),
(3, 'rafal@rafal.pl', '1990-02-05', 'bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla ', 453654444, 'Mężczyzna');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `menu_glowne`
--

CREATE TABLE `menu_glowne` (
  `id_menu_strony` int(11) NOT NULL,
  `nazwa` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `menu_glowne`
--

INSERT INTO `menu_glowne` (`id_menu_strony`, `nazwa`) VALUES
(10, 'Kontakt');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `news`
--

CREATE TABLE `news` (
  `id_newsa` int(11) NOT NULL,
  `kto_dodal` int(11) DEFAULT NULL,
  `kiedy` datetime NOT NULL,
  `tytul_newsa` text COLLATE utf8_polish_ci NOT NULL,
  `tresc_newsa` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `news`
--

INSERT INTO `news` (`id_newsa`, `kto_dodal`, `kiedy`, `tytul_newsa`, `tresc_newsa`) VALUES
(4, 1, '2019-05-25 23:05:40', 'Luis Suárez: Operacja mojego kolana nie miala nic wspólnego z problemami z chrząstką', 'Luis Suárez nie mógł zagrać w finale Pucharu Króla z Valencią i obejrzał spotkanie z trybun Benito Villamarín. Po meczu Urugwajczyk zamieścił post na Instagramie, w którym wyjaśnił, dlaczego poddał się operacji kolana przed zakończeniem sezonu.\r\n\r\nTo dzień pełen smutku i rozczarowania, ponieważ nie mogłem pomóc moim kolegom, ale tak, jestem z nich wszystkich bardzo dumny, ponieważ wczoraj próbowali wszystkiego, co było możliwe, żeby dać naszym kibicom radość.\r\n\r\nTeraz nadszedł czas, by zmienić nastawienie przed kolejnym sezonem i zrobić wszystko, żeby osiągnąć nasze cele.\r\n\r\nNie lubię zbytnio składać wyjaśnień w sprawie tego, co się o mnie mówi i co dociera do ludzi, ale teraz czuję się w obowiązku to zrobić, ponieważ sądzę, że wiele rzeczy mówi się w złej intencji i podważają one mój profesjonalizm.\r\n\r\nRozpocząłem ten sezon z dyskomfortem w chrząstce w kolanie, o czym wielu ludzi już wie, i dzięki WIELKIEJ PRACY sztabu medycznego byłem w stanie wytrzymać bez żadnego problemu. Dziękuję im wszystkim za pracę i poświęcenie.\r\n\r\nZ tego powodu chciałbym wyjaśnić, szczególnie tym, którzy wydaje się, że chcą mnie zranić, że kontuzja, która doprowadziła mnie do operacji, nie miała absolutnie nic wspólnego z chrząstką, tylko z zerwaniem łąkotki, którego doznałem w meczu z Liverpoolem. Przez nią zostałem zmuszony do przejścia operacji i opuszczenia wczorajszego finału wbrew mojej woli.\r\n\r\nOdkąd trafiłem do tego klubu, pokazywałem każdego dnia, że jestem w stu procentach zaangażowany, daję z siebie wszystko na każdym treningu i w każdym meczu w obronie tego herbu, ponieważ zawsze marzyłem, żeby się tutaj dostać! Bardzo dziękuję wszystkim i NAPRZÓD BARCA!\r\n\r\nLuis Suárez,\r\nBarcelona, 26 maja 2019 r.'),
(27, 1, '2019-05-26 14:57:42', 'Barcelona', 'Przegrywa :( O nie :( 2-1'),
(29, 1, '2019-06-01 16:11:04', 'Mundo Deportivo: Messi potrzebuje wsparcia strzeleckiego, aby Barça mogła sięgnąć po tryplet ', 'Leo Messi zawsze jest nastawiony na zdobywanie bramek, a jego zdolności są niepodważalne, ale nawet taki geniusz jak Argentyńczyk potrzebuje wsparcia strzeleckiego, jeśli Barcelona ma aspirować do trypletu. W niedawno zakończonym sezonie Blaugrana była bliska wygrania Ligi Mistrzów i Pucharu Króla, co dałoby jej upragnione trzy trofea, ale Katalończykom brakowało goli i to nie z winy kapitana zespołu. Mundo Deportivo porównało dorobek strzelecki obecnego tridente ze statystykami poprzednich ataków Barçy, gdy drużyna sięgała po triumf w Lidze Mistrzów.\r\n\r\nW sezonie 2008/2009 Messi strzelił zaledwie dwa gole więcej niż Samuel Eto’o (38-36). Tridente ofensywne uzupełniał Thierry Henry, autor 26 trafień. Łącznie zdobyli oni 100 bramek, a w samej tylko Lidze Mistrzów - 21. Podczas drugiego trypletu w rozgrywkach 2014/2015 napastnicy Barcelony strzelili jeszcze więcej goli. 58 trafień Messiego, 39 Neymara i 25 Luisa Suáreza dały łącznie Blaugranie 122 bramki. W samej tylko Lidze Mistrzów członkowie tridente 27 razy wpisywali się na listę strzelców.\r\n\r\nBarça mogła też zdobyć trzy trofea w sezonie 2010/2011, ale przegrała w finale Pucharu Króla. Katalończycy sięgnęli jednak po triumf w Lidze Mistrzów i po mistrzostwo Hiszpanii. Atak drużyny tworzyli wówczas Leo Messi, David Villa i Pedro. Argentyńczyk strzelił 53 bramki, a Hiszpanie odpowiednio po 23 i 22 gole. W Lidze Mistrzów to tridente zanotowało 21 trafień.\r\n\r\nA jak zakończył się sezon 2018/2019? Messi strzelił 51 goli, z czego dwanaście w Lidze Mistrzów, gdzie miał na koncie o dwa trafienia więcej niż w rozgrywkach 2014/2015, o trzy bramki więcej niż w sezonie 2008/2009 i taki sam dorobek jak w kampanii 2010/2011. Problemem jest więc wkład pozostałych zawodników. Ousmane Dembélé strzelił w Champions League trzy gole, a Luis Suárez tylko jednego, więc tridente miało na koncie szesnaście trafień. Do tego można dołożyć Philippe Coutinho, autora trzech bramek. Czterech ofensywnych piłkarzy Barcelony strzeliło w tych rozgrywkach mniej goli niż tridente w każdym z trzech wymienionych sezonów i to przy lepszej skuteczności Messiego. Łącznie Messi, Suárez i Dembélé zdobyli w ostatnim sezonie 90 bramek.'),
(31, 1, '2019-07-03 13:09:55', 'BARÇA ODRZUCA OFERTĘ BAYERNU ZA DEMBÉLÉ', 'Bayern Monachium podjął pierwszą próbę sprowadzenia Ousmane\'a Dembélé. Barcelona odrzuciła jednak ofertę Bawarczyków opiewającą na 70 milionów euro. Włodarze niemieckiego klubu wykonali już pierwszy krok, aby zorientować się jaka jest sytuacja napastnika, ale zaoferowana kwota jest o wiele niższa od tej wymaganej przez Katalończyków. Biorąc pod uwagę to, że Barcelona pozyskała Francuza z Borussii Dortmund za 105 milionów euro (+ 40 milionów zmiennych), mistrzowie Hiszpanii nie sprzedadzą piłkarza za mniej niż 95 milionów euro.\r\n\r\n\r\nBayern szuka napastnika, aby załatać dziury po odejściu Robbena oraz Ribéry\'ego i Dembélé jest jednym z obserwowanych zawodników. Na liście znajduje się również Leroy Sané, ale Manchester City wycenił go na kwotę 100 milionów euro, na co Bawarczycy nie chcą się zgodzić.\r\n\r\nNiemiecki klub kontaktował się już z agentem napastnika Barcelony. Dembélé jest otwarty na zmianę otoczenia i opuszczenie Katalonii jeśli zaproponowana oferta będzie interesująca, ale nie odrzuca możliwości kontynuowania kariery na Camp Nou.\r\n\r\nBarcelona czeka, aż Bayern podniesie ofertę. Dembélé może również zostać włączony w operację sprowadzenia Neymara, ale Duma Katalonii wolałaby sprzedać go Bawarczykom i tym samym bezpośrednio otrzymać pieniądze z transferu.');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pilkarze`
--

CREATE TABLE `pilkarze` (
  `id_pilkarza` int(11) NOT NULL,
  `imie_pilkarza` text COLLATE utf8_polish_ci NOT NULL,
  `nazwisko_pilkarza` text COLLATE utf8_polish_ci NOT NULL,
  `pozycja_pilkarza` text COLLATE utf8_polish_ci NOT NULL,
  `numer_pilkarza` int(11) NOT NULL,
  `ilosc_bramek` int(11) NOT NULL DEFAULT '0',
  `ilosc_asyst` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `pilkarze`
--

INSERT INTO `pilkarze` (`id_pilkarza`, `imie_pilkarza`, `nazwisko_pilkarza`, `pozycja_pilkarza`, `numer_pilkarza`, `ilosc_bramek`, `ilosc_asyst`) VALUES
(1, 'Lionel', 'Messi', 'Napastnik', 10, 9, 2),
(2, 'Luis', 'Suarez', 'Napastnik', 9, 10, 2),
(4, 'Ousmane', 'Dembele', 'Napastnik', 11, 11, 5);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `username` varchar(30) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `avatar_name` varchar(255) COLLATE utf8_polish_ci DEFAULT 'default.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id_user`, `username`, `email`, `password`, `admin`, `avatar_name`) VALUES
(1, 'admin', 'kosektomasz96@gmail.com', '0192023a7bbd73250516f069df18b500', 1, 'default.png'),
(2, 'Test', 'test@wp.pl', '16d7a4fca7442dda3ad93c9a726597e4', 0, 'default.png'),
(5, 'Kosek', 'test666@wp.pl', 'bb0026d5618e55745b3f7cf0e875c67e', 0, 'default.png');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `kontakt`
--
ALTER TABLE `kontakt`
  ADD PRIMARY KEY (`id_kontaktu`);

--
-- Indeksy dla tabeli `menu_glowne`
--
ALTER TABLE `menu_glowne`
  ADD PRIMARY KEY (`id_menu_strony`);

--
-- Indeksy dla tabeli `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id_newsa`);

--
-- Indeksy dla tabeli `pilkarze`
--
ALTER TABLE `pilkarze`
  ADD PRIMARY KEY (`id_pilkarza`);

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `kontakt`
--
ALTER TABLE `kontakt`
  MODIFY `id_kontaktu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `menu_glowne`
--
ALTER TABLE `menu_glowne`
  MODIFY `id_menu_strony` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `news`
--
ALTER TABLE `news`
  MODIFY `id_newsa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT dla tabeli `pilkarze`
--
ALTER TABLE `pilkarze`
  MODIFY `id_pilkarza` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
