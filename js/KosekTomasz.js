var context;
var dx= 4;
var dy=4;
var y=140;
var x=40;
var wyniczek;
var wynik1 = 0;
var wynik2 = 0;
var wyn1 = 1;
var wyn2 = 0;
//var dane = document.getElementById("dane").innerHTML;


function draw(){
	y = y + Math.sin(x/30)*0.5;
	context= myCanvas.getContext('2d');
	kropka1 = myCanvas.getContext('2d');
	kropka2 = myCanvas.getContext('2d');
	kropka3 = myCanvas.getContext('2d');
	kropka4 = myCanvas.getContext('2d');
	kropka5 = myCanvas.getContext('2d');
	kropka6 = myCanvas.getContext('2d');
	kropka7 = myCanvas.getContext('2d');
	kropka8 = myCanvas.getContext('2d');
	//kropka9 = myCanvas.getContext('2d');
	bramka1 = myCanvas.getContext("2d");
	bramka2 = myCanvas.getContext("2d");
	srodekokrag = myCanvas.getContext("2d");
	srodek = myCanvas.getContext("2d");
	liniasrodek = myCanvas.getContext("2d");
	liniagora = myCanvas.getContext("2d");
	liniadol = myCanvas.getContext("2d");
	liniaprawa = myCanvas.getContext("2d");
	linialewa = myCanvas.getContext("2d");
	karne1 = myCanvas.getContext("2d");
	karne2 = myCanvas.getContext("2d");
	karne3 = myCanvas.getContext("2d");
	karne4 = myCanvas.getContext("2d");
	karne5 = myCanvas.getContext("2d");
	karne6 = myCanvas.getContext("2d");
	karnekropka1 = myCanvas.getContext("2d");
	karnekropka2 = myCanvas.getContext("2d");
	lewapolokrag = myCanvas.getContext("2d");
	prawapolokrag = myCanvas.getContext("2d");
	flagalewadol = myCanvas.getContext("2d");
	flagalewagora = myCanvas.getContext("2d");
	flagaprawadol = myCanvas.getContext("2d");
	flagaprawagora = myCanvas.getContext("2d");
	wyniczek = myCanvas2.getContext("2d");
	context.clearRect(0,0,450,300);
	
	
	
	
	//boisko
	//bramka1
	bramka1.beginPath();
	bramka1.fillStyle="white";
	bramka1.rect(0,105,20,90);
	bramka1.strokeStyle ="#333333";
	bramka1.stroke();
	bramka1.closePath();
	bramka1.fill();
	//bramka2
	bramka2.beginPath();
	bramka2.fillStyle="white";
	bramka2.rect(430,105,20,90);
	bramka2.strokeStyle ="#333333";
	bramka2.stroke();
	bramka2.closePath();
	bramka2.fill();
	//srodek boiska okrag
	srodekokrag.beginPath();
	srodekokrag.fillStyle="none";
	srodekokrag.arc(225,150,32,0,2*Math.PI);
	srodekokrag.strokeStyle ="white";
	srodekokrag.stroke();
	srodekokrag.closePath();
	//srodek boiska kropka
	srodek.beginPath();
	srodek.fillStyle="none";
	srodek.arc(225,150,4,0,2*Math.PI);
	srodek.strokeStyle ="white";
	srodek.stroke();
	srodek.fill();
	srodek.closePath();
	//linia srodek
	liniasrodek.beginPath();
	liniasrodek.moveTo(225,5);
	liniasrodek.lineTo(225,295);
	liniasrodek.strokeStyle ="white";
	liniasrodek.stroke();
	liniasrodek.closePath();
	//linia gora
	liniagora.beginPath();
	liniagora.moveTo(5,5);
	liniagora.lineTo(445,5);
	liniagora.strokeStyle ="white";
	liniagora.stroke();
	liniagora.closePath();
	//linia dol
	liniadol.beginPath();
	liniadol.moveTo(5,295);
	liniadol.lineTo(445,295);
	liniadol.strokeStyle ="white";
	liniadol.stroke();
	liniadol.closePath();
	//linia lewa
	linialewa.beginPath();
	linialewa.moveTo(5,5);
	linialewa.lineTo(5,295);
	linialewa.strokeStyle ="white";
	linialewa.stroke();
	linialewa.closePath();
	//linia prawa
	liniaprawa.beginPath();
	liniaprawa.moveTo(445,5);
	liniaprawa.lineTo(445,295);
	liniaprawa.strokeStyle ="white";
	liniaprawa.stroke();
	liniaprawa.closePath();
	//////////////
	
	//pole karne
	//karne 1
	karne1.beginPath();
	karne1.moveTo(50,90);
	karne1.lineTo(50,210);
	karne1.strokeStyle ="white";
	karne1.stroke();
	karne1.closePath();
	//karne2
	karne2.beginPath();
	karne2.moveTo(5,90);
	karne2.lineTo(50,90);
	karne2.strokeStyle ="white";
	karne2.stroke();
	karne2.closePath();
	//karne 3
	karne3.beginPath();
	karne3.moveTo(5,210);
	karne3.lineTo(50,210);
	karne3.strokeStyle ="white";
	karne3.stroke();
	karne3.closePath();
	//karne 4
	karne4.beginPath();
	karne4.moveTo(400,90);
	karne4.lineTo(400,210);
	karne4.strokeStyle ="white";
	karne4.stroke();
	karne4.closePath();
	//karne 5
	karne5.beginPath();
	karne5.moveTo(400,90);
	karne5.lineTo(445,90);
	karne5.strokeStyle ="white";
	karne5.stroke();
	karne5.closePath();
	//karne 6
	karne6.beginPath();
	karne6.moveTo(400,210);
	karne6.lineTo(445,210);
	karne6.strokeStyle ="white";
	karne6.stroke();
	karne6.closePath();
	//karne kropka lewa
	karnekropka1.beginPath();
	karnekropka1.fillStyle="none";
	karnekropka1.arc(40,150,2,0,2*Math.PI);
	karnekropka1.strokeStyle ="white";
	karnekropka1.stroke();
	karnekropka1.fill();
	karnekropka1.closePath();
	//karne kropka prawa
	karnekropka2.beginPath();
	karnekropka2.fillStyle="none";
	karnekropka2.arc(410,150,2,0,2*Math.PI);
	karnekropka2.strokeStyle ="white";
	karnekropka2.stroke();
	karnekropka2.fill();
	karnekropka2.closePath();
	//polkole lewa
	lewapolokrag.beginPath();
	lewapolokrag.fillStyle="none";
	lewapolokrag.arc(50,150,32,1.5*Math.PI,0.5*Math.PI);
	lewapolokrag.strokeStyle ="white";
	lewapolokrag.stroke();
	lewapolokrag.closePath();
	//polkole prawa
	prawapolokrag.beginPath();
	prawapolokrag.fillStyle="none";
	prawapolokrag.arc(400,150,32,0.5*Math.PI,1.5*Math.PI);
	prawapolokrag.strokeStyle ="white";
	prawapolokrag.stroke();
	prawapolokrag.closePath();
	
	
	
	//////////////
	//piłka
	//duza piłka
	context.beginPath();
	context.fillStyle="white";
	context.arc(x,y,15,0,Math.PI*2,true);
	context.strokeStyle="#333333";
	context.stroke();
	context.fill();
	context.closePath();
	
	// pierwsza kropka
	kropka1.beginPath();
	kropka1.fillStyle="#333333";
	kropka1.arc(x+10,y-3,4,0,Math.PI*2,true);
	kropka1.strokeStyle="#333333";
	kropka1.stroke();
	kropka1.fill();
	//druga kropka
	kropka2.beginPath();
	kropka2.fillStyle="#333333";
	kropka2.arc(x-10,y+4,4,0,Math.PI*2,true);
	kropka2.strokeStyle="#333333";
	kropka2.stroke();
	kropka2.fill();
	//trzecia kropka
	kropka3.beginPath();
	kropka3.fillStyle="#333333";
	kropka3.arc(x-4,y-11,3,0,Math.PI*2,true);
	kropka3.strokeStyle="#333333";
	kropka3.stroke();
	kropka3.fill();
	//czwarta kropka
	kropka4.beginPath();
	kropka4.fillStyle="#333333";
	kropka4.arc(x,y+12,3,0,Math.PI*2,true);
	kropka4.strokeStyle="#333333";
	kropka4.stroke();
	kropka4.fill();
	//piata kropka
	kropka5.beginPath();
	kropka5.fillStyle="#333333";
	kropka5.arc(x,y,3,0,Math.PI*2,true);
	kropka5.strokeStyle="#333333";
	kropka5.stroke();
	kropka5.fill();
	//szósta kropka
	kropka6.beginPath();
	kropka6.fillStyle="#333333";
	kropka6.arc(x+9,y+9,2,0,Math.PI*2,true);
	kropka6.strokeStyle="#333333";
	kropka6.stroke();
	kropka6.fill();
	//siódma kropka
	kropka7.beginPath();
	kropka7.fillStyle="#333333";
	kropka7.arc(x+5,y-12,2,0,Math.PI*2);
	kropka7.strokeStyle="#333333";
	kropka7.stroke();
	kropka7.fill();
	//kropka7.closePath();
	//ósma kropka
	
	kropka8.beginPath();
	kropka8.fillStyle= "#333333";
	kropka8.arc(x-12,y-5,2,0,Math.PI*2);
	kropka8.fill();
	kropka8.strokeStyle="#333333";
	kropka8.stroke();
	kropka8.closePath();
	//kropka8.closePath();
	
	//flaga lewa dol
	flagalewadol.beginPath();
	flagalewadol.moveTo(5+x/200,297+x/110);
	flagalewadol.lineTo(20+x/200,293+x/110);
	flagalewadol.lineTo(5+x/200,284+x/110);
	flagalewadol.fillStyle = "red";
	flagalewadol.fill();

	
	//flaga lewa gora
	flagalewagora.moveTo(5+x/200,2-x/110);
	flagalewagora.lineTo(20+x/200,8-x/110);
	flagalewagora.lineTo(5+x/200,15-x/110);
	flagalewagora.fillStyle = "red";
	flagalewagora.fill();

	
	//flaga prawa dol
	flagaprawadol.moveTo(445-x/200,295-x/110);
	flagaprawadol.lineTo(430-x/200,291-x/110);
	flagaprawadol.lineTo(445-x/200,282-x/110);
	flagaprawadol.fillStyle = "red";
	flagaprawadol.fill();

	
	//flaga prawa gora
	flagaprawagora.moveTo(445-x/200,2+x/110);
	flagaprawagora.lineTo(430-x/200,8+x/110);
	flagaprawagora.lineTo(445-x/200,15+x/110);
	flagaprawagora.fillStyle = "red";
	flagaprawagora.fill();
	
	
	
	if( x<40 || x>410)
	{
		dx=-dx;
		if(wyn1 == 1)
		{
				wynik1++;
				wyn1 = 0;
				wyn2 = 1;
		}
		else
		{
			wynik2++;
			wyn1=1;
			wyn2=0;
		}
		
	}
	var doc1 = document.getElementById("wynik");
	var doc2 = document.getElementById("wynik2");
	doc1.innerHTML= wynik1 + " -";
	doc1.text = "&nbsp;" + wynik1;
	doc2.innerHTML = "&nbsp;" + wynik2;
	doc2.text = wynik1;
	
	if( y<0 || y>300)
		dy=-dy;
		x+=dx;
	}
	
setInterval(draw,15); 
