<?php
if ($zalogowany && $_SESSION['admin']==1){

?>
<h2 class="ui header">Zarządzanie stronami</h2><br />

<?php
  switch(@$params[0])
  {
    case 'usun':
      $id_do_usuniecia = $params[1];
      if($db->query('delete from menu_glowne where id_menu_strony=' . $id_do_usuniecia))
      {
        header("Location: /barca/adminpanel/strony");
      }
      break;

    case 'edytuj':
      $id_do_edycji = $params[1];
      if($db->query('update menu_glowne set nazwa="' . $_POST['nazwa'] . '" where id_menu_strony=' . $id_do_edycji))
      {
        header("Location: /barca/adminpanel/strony");
      }
      break;

    case 'dodaj':
      if($db->query('insert into menu_glowne values(null, "' . $_POST['nazwa'] . '")'))
      {
		  //$link = str_replace('/barca/', '', $_SERVER['REQUEST_URI']);
		//echo $link;
			if(@$_SESSION['admin']==1 && $zalogowany)
			{
				$nazwa='<a href="./edytujstrone/'.$_POST['nazwa'].'"><button class="ui black button"><i class="pencil icon"></i>Edytuj tą stronę</button></a><br/><br/>';	
			}
			$plik = $_POST['nazwa'].'.php';
			$fp = fopen($plik, 'w');
			fwrite($fp, $nazwa);
			fclose($fp);
        header("Location: /barca/adminpanel/strony");
      }
      break;

  }
  $strony_query = 'select * from menu_glowne order by nazwa;';
  $str_array = $db->query($strony_query);
?>
<form method="post" class="ui form" action="./adminpanel/strony/dodaj">
  <div class="inline fields">
      <div class="eight wide field">
        <input type="text" name="nazwa" placeholder="Nazwa Strony" />
      </div>
      <div class="field">
        <button type="submit" class="ui green labeled icon button">
          <i class="add icon"></i>
          Dodaj nową stronę
        </button>
      </div>
  </div>
</form>

<table class="ui celled table">
  <thead>
    <tr>
      <th>Nazwa strony</th>
      <th>Akcje</th>
    </tr>
  </thead>
<?php
  foreach($str_array as $k)
  {
    echo '<tr>';
    echo '<td>' . $k['nazwa'] . '</td>';
    echo '<td class="right aligned collapsing">';
    echo '<a onclick="$(\'.ui.modal.do-edycji.' . $k['id_menu_strony'] . '\').modal(\'show\');" class="ui primary labeled icon button"><i class="pencil icon"></i>edytuj</a>
          <a onclick="$(\'.ui.basic.modal.do-usuniecia.' . $k['id_menu_strony'] . '\').modal(\'show\');" class="ui red labeled icon button"><i class="trash icon"></i>usuń</a>';
    echo '<div class="ui basic modal do-usuniecia ' . $k['id_menu_strony'] . '">
            <div class="ui icon header">
              <i class="trash alternate icon"></i>
              Usunąć stronę "' . $k['nazwa'] . '"?
            </div>
            <div class="content">
              <p>Operacja jest nieodwracalna.</p>
            </div>
            <div class="actions">
              <div class="ui green cancel inverted button">
                <i class="remove icon"></i>
                Nie
              </div>
              <a href="./adminpanel/strony/usun/' . $k['id_menu_strony'] . '" class="ui red labeled icon ok button">
                <i class="trash alternate icon"></i>
                Tak
              </a>
            </div>
          </div>


          <form class="ui form modal do-edycji ' . $k['id_menu_strony'] . '" action="./adminpanel/strony/edytuj/' . $k['id_menu_strony'] . '" method="post">
            <div class="header">
              Zmień nazwę dla "' . $k['nazwa'] . '"
            </div>
            <div class="content">
              <input type="text" name="nazwa" placeholder="Nowa nazwa..." />
            </div>
            <div class="actions">
              <div class="ui black deny button">
                Anuluj
              </div>
              <button type="submit" class="ui positive right labeled icon button">
                Zapisz
                <i class="save icon"></i>
              </div>
            </div>
          </form>';

    echo '</td>';
    echo '</tr>';
  }
echo "</table>";

}
else{
	echo '<h1 class="ui header">Nie masz dostępu do tej części serwisu</h1>';
}

?>