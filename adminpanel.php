<?php
  $pozycje_menu = array(array('alias' => 'dodajnewsa', 'nazwa' => 'Dodaj Newsa'), array('alias' => 'newsy', 'nazwa' => 'Newsy na stronie'), array('alias' => 'zgloszenia', 'nazwa' => 'Zgłoszenia'), array('alias' => 'uzytkownicy', 'nazwa' => 'Użytkownicy'), array('alias' => 'strony', 'nazwa' => 'Strony'));
  if(!empty($params[0]))
  {
    $active = array_shift($params);
  }
  else
  {
    $active = 'dodajnewsa';
  }
?>
<article>
  <h1 class="ui dividing header">Panel Administracyjny</h1>
  <div class="ui two column stackable grid">
    <div class="four wide column">
      <div class="ui secondary vertical pointing menu">
        <?php
          foreach($pozycje_menu as $men)
          {
            echo '<a class="';
            if($men['alias'] == $active) echo 'active ';
            echo 'item" href="./adminpanel/';
            echo $men['alias'];
            echo '">';
            echo $men['nazwa'];
            echo '</a>';
          }
         ?>
      </div>
    </div>
    <div class="twelve wide column">
      <?php include 'adminpanel.' . $active . '.php'; ?>
    </div>
  </div>
</article>
