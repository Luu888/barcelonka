<article>
<?php
if(@$_SESSION['admin']==1)
	{
		echo '<a href="./dodajpilkarza"><button class="ui green button"><i class="ui plus icon"></i>Dodaj nowego piłkarza</i></button></a>';
	}

?>
	<table class="ui celled table center aligned">
		<thead>
			<tr>
				<th>Numer na koszulce</th>
				<th>Imię i nazwisko</th>
				<th>Pozycja</th>
				<th>Ilość bramek</th>
				<th>Ilość asyst</th>
				<?php
					if(@$_SESSION['admin']==1)
						{
							echo '<th>Edytuj</th>';
							echo '<th>Usuń piłkarza</th>';
						}
				?>
			</tr>
		</thead>
		<tbody>
		<?php
		$query ='select * from pilkarze order by numer_pilkarza';
		$query_pilkarze=$db->query($query);
		foreach($query_pilkarze as $p)
			{	
					echo '<tr>';
					echo '<td data-label="numer">'.$p['numer_pilkarza'].'</td>';
					echo '<td data-label="imienazwisko">'.$p['imie_pilkarza'].' '.$p['nazwisko_pilkarza'].'</td>';
					echo '<td data-label="pozycja">'.$p['pozycja_pilkarza'].'</td>';
					if(@$_SESSION['admin']==1)
						{
							echo '<td data-label="bramki"><a href="./pilkarze/'.$p['id_pilkarza'].'/usunbramke"><i class="minus alternate icon"></i></a>'.$p['ilosc_bramek'].'<a href="./pilkarze/'.$p['id_pilkarza'].'/dodajbramke"><i class="plus alternate icon"></i></a></td>';
							echo '<td data-label="asysty"><a href="./pilkarze/'.$p['id_pilkarza'].'/usunasyste"><i class="minus alternate icon"></i></a>'.$p['ilosc_asyst'].'<a href="./pilkarze/'.$p['id_pilkarza'].'/dodajasyste"><i class="plus alternate icon"></i></a></td>';
						}
						else
						{
							echo '<td data-label="bramki">'.$p['ilosc_bramek'].'</td>';
							echo '<td data-label="asysty">'.$p['ilosc_asyst'].'</td>';
								
						}
					if(@$_SESSION['admin']==1)
						{
							echo '<td data-label="edycja"><a href="./edytujpilkarza/'.$p['id_pilkarza'].'"><i class="pencil alternate icon"></i></td>';
							echo '<td data-label="edycja"><a href="./pilkarze/'.$p['id_pilkarza'].'/usun"><i class="trash alternate icon"></i></td>';
						}
					echo '</tr>';
					
			}
		
		?>
		</tbody>

	</table>


</article>
<?php
if(isset($params[1]) && $params[1] == 'usun')
{
	if($zalogowany && $_SESSION['admin']==1)
	{
		$query_likes = 'delete from pilkarze where id_pilkarza='.$params[0];
		$db->query($query_likes);
	header('Location: /barca/pilkarze/');
}}
if(isset($params[1]) && $params[1] == 'usunbramke')
{
	if($zalogowany && $_SESSION['admin']==1)
	{
		$query_likes = 'update pilkarze set ilosc_bramek=ilosc_bramek-1 where id_pilkarza='.$params[0];
		$db->query($query_likes);
	header('Location: /barca/pilkarze/');
}}
if(isset($params[1]) && $params[1] == 'usunasyste')
{
	if($zalogowany && $_SESSION['admin']==1)
	{
		$query_likes = 'update pilkarze set ilosc_asyst=ilosc_asyst-1 where id_pilkarza='.$params[0];
		$db->query($query_likes);
	header('Location: /barca/pilkarze/');
}}
if(isset($params[1]) && $params[1] == 'dodajbramke')
{
	if($zalogowany && $_SESSION['admin']==1)
	{
		$query_likes = 'update pilkarze set ilosc_bramek=ilosc_bramek+1 where id_pilkarza='.$params[0];
		$db->query($query_likes);
	header('Location: /barca/pilkarze/');
}}
if(isset($params[1]) && $params[1] == 'dodajasyste')
{
	if($zalogowany && $_SESSION['admin']==1)
	{
		$query_likes = 'update pilkarze set ilosc_asyst=ilosc_asyst+1 where id_pilkarza='.$params[0];
		$db->query($query_likes);
	header('Location: /barca/pilkarze/');
}}
?>