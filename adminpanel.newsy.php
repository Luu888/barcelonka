<?php
if ($zalogowany && $_SESSION['admin']==1){

?>
<h2 class="ui header">Zarządzanie stronami</h2><br />

<?php
  switch(@$params[0])
  {
    case 'usun':
      $id_do_usuniecia = $params[1];
      if($db->query('delete from news where id_newsa=' . $id_do_usuniecia))
      {
        header("Location: /barca/adminpanel/newsy");
      }
      break;

    case 'edytuj':
      $id_do_edycji = $params[1];
      if($db->query('update news set tytul="' . $_POST['nazwa'] . '" where id_newsa=' . $id_do_edycji))
      {
        header("Location: /barca/adminpanel/newsy");
      }
      break;

  }
  $strony_query = 'select * from news order by id_newsa;';
  $str_array = $db->query($strony_query);
?>

<table class="ui celled table">
  <thead>
    <tr>
      <th>Tytuł Newsa</th>
      <th>Akcje</th>
    </tr>
  </thead>
<?php
  foreach($str_array as $k)
  {
    echo '<tr>';
    echo '<td>' . $k['tytul_newsa'] . '</td>';
    echo '<td class="right aligned collapsing">';
    echo '<a onclick="$(\'.ui.modal.do-edycji.' . $k['id_newsa'] . '\').modal(\'show\');" class="ui primary labeled icon button"><i class="pencil icon"></i>edytuj</a>
          <a onclick="$(\'.ui.basic.modal.do-usuniecia.' . $k['id_newsa'] . '\').modal(\'show\');" class="ui red labeled icon button"><i class="trash icon"></i>usuń</a>';
    echo '<div class="ui basic modal do-usuniecia ' . $k['id_newsa'] . '">
            <div class="ui icon header">
              <i class="trash alternate icon"></i>
              Usunąć stronę "' . $k['tytul_newsa'] . '"?
            </div>
            <div class="content">
              <p>Operacja jest nieodwracalna.</p>
            </div>
            <div class="actions">
              <div class="ui green cancel inverted button">
                <i class="remove icon"></i>
                Nie
              </div>
              <a href="./adminpanel/newsy/usun/' . $k['id_newsa'] . '" class="ui red labeled icon ok button">
                <i class="trash alternate icon"></i>
                Tak
              </a>
            </div>
          </div>


          <form class="ui form modal do-edycji ' . $k['id_newsa'] . '" action="./adminpanel/newsy/edytuj/' . $k['id_newsa'] . '" method="post">
            <div class="header">
              Zmień nazwę dla "' . $k['tytul_newsa'] . '"
            </div>
            <div class="content">
              <input type="text" name="nazwa" placeholder="Nowa nazwa..." />
            </div>
            <div class="actions">
              <div class="ui black deny button">
                Anuluj
              </div>
              <button type="submit" class="ui positive right labeled icon button">
                Zapisz
                <i class="save icon"></i>
              </div>
            </div>
          </form>';

    echo '</td>';
    echo '</tr>';
  }
echo "</table>";

}
else{
	echo '<h1 class="ui header">Nie masz dostępu do tej części serwisu</h1>';
}

?>