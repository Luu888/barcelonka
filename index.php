<?php
session_start();
ob_start();
error_reporting(E_ALL);
//require('funkcje.php');
if(!isset($_SESSION['zalogowany'])) $_SESSION['zalogowany'] = 0;

$request = str_replace('/barca/', '', $_SERVER['REQUEST_URI']);
$params = explode('/', $request);

if($params[0] == 'wyloguj' && $_SESSION['zalogowany'] == 1){
	session_destroy();
	header("Location: /barca/");
}

$db = new mysqli('localhost', 'root', '', 'barcelona');
$db->set_charset("utf8");

$site = 'glowna';
if(!empty($params[0]))
	$site = array_shift($params);
$site_filename = $site . '.php';
if(!is_file($site_filename))
	$site_filename = 'blad.php';

$zalogowany = $_SESSION['zalogowany'] == 1;
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<base href="http://localhost/barca/">
	<link rel="stylesheet" href="styles/styles.css">
	<link rel="stylesheet" href="styles/stopka.css">
	<link rel="stylesheet" href="semantic/semantic.min.css">
	<link rel="stylesheet" href="styles/galeria.css">
	<script src="jquery-3.3.1.min.js"></script>
	<script src="semantic/semantic.min.js"></script>
	<link rel="icon" type="image/ico"  href="./images/favicon.ico">
	<script src="js/script.js"></script>
	<!--<script src="js/KosekTomasz.js"></script>-->
	<title>FC Barcelona</title>

</head>
<body>


	<div class="ui red fixed inverted main menu" style="width: 100%;">
		<a href="./" class="item">Strona Główna</a>
		<a href="./newsy" class="item">Newsy</a>
		<a href="./oklubie" class="item">O klubie</a>
		<a href="./pilkarze" class="item">Piłkarze</a>
		<div class="menu">
					<?php
						$strony_q = 'select * from menu_glowne order by id_menu_strony asc;';
						$str_q = $db->query($strony_q);
						foreach($str_q as $k)
						{
							echo '<a href="./'.$k['nazwa'].'" class="item">' . $k['nazwa'] . '</a>';
						}
					?>
				</div>
		<div class="right menu">
		<?php
		if (!$zalogowany)
			{
				echo '<a href="./logowanie" class="item">Logowanie</a>';
			}	
		else 
			{
				if($_SESSION['admin']==1)
					{
						echo '<a href="./adminpanel" class="item"><i><b>Panel Admina</b></i></a>';
						//echo '<a href="./wiadomosci" class="red item">Zgłoszenia</a>';
					}
				echo '<a href="./wyloguj" class="item">Wyloguj</a>';
			}
		?>
		</div>
	</div>
	<div class="ui red inverted fixed menu hamburger">
<a class="item" onclick="$('.ui.sidebar').sidebar('toggle');">
	<i class="sidebar icon"></i>
	Menu
</a>
<div class="ui red sidebar inverted vertical accordion menu">
<a href="./" class="item">Strona Główna</a>
		<a href="./newsy" class="red item">Newsy</a>
		<a href="./oklubie" class="red item">O klubie</a>
		<a href="./pilkarze" class="red item">Piłkarze</a>
		<?php
						$strony_q = 'select * from menu_glowne order by id_menu_strony asc;';
						$str_q = $db->query($strony_q);
						foreach($str_q as $k)
						{
							echo '<a href="./'.$k['nazwa'].'" class="item">' . $k['nazwa'] . '</a>';
						}
					?>
		<?php
		if (!$zalogowany)
			{
				echo '<a href="./logowanie" class="red item">Logowanie</a>';
			}	
		else 
			{
				if($_SESSION['admin']==1)
					{
						echo '<a href="./adminpanel" class="red item"><b><i>Panel Admina</i></b></a>';
						//echo '<a href="./wiadomosci" class="red item"><b>Zgłoszenia</b></a>';
					}
				echo '<a href="./wyloguj" class="red item"><b>Wyloguj</b></a>';
			}
		?>
		
</div>
</div>
	<div class="ui container" style="margin-top: 100px; flex: 1;">
	<?php
		$link = str_replace('/barca/', '', $_SERVER['REQUEST_URI']);
		//echo $link;
		if(@$_SESSION['admin']==1 && $zalogowany)
			{
				if($link=='oklubie')
					{
						echo '<a href="./edytujstrone/'.$link.'"><button class="ui black button"><i class="pencil icon"></i>Edytuj tą stronę</button></a><br/><br/>';	
					}
			}?>
	<?php		
		include $site_filename; 
	?>
	</div>
	<div class="ui blue inverted vertical footer segment" style="margin-top: 100px; padding: 50px;" id="stopka">
		<div class="ui container">
			<div class="ui stackable inverted divided equal height stackable grid centered">
				Copyright © 2019 Kosek Tomasz 
			</div>
		</div>
	</div>
	
	<script>
		$('.ui.dropdown').dropdown();
	</script>
</body>
</html>