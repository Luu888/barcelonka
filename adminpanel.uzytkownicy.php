<?php
if(!empty($params[0]))
  switch($params[0])
  {
    case 'usun':
      $id_do_usuniecia = $params[1];
      if($db->query('delete from users where id_user=' . $id_do_usuniecia))
      {
        header("Location: /barca/adminpanel/uzytkownicy");
      }
      break;

    case 'edytuj':
      $id_do_edycji = $params[1];
      if($db->query('update users set username="' . $_POST['nazwa'] . '" where id_user=' . $id_do_edycji))
      {
        header("Location: /barca/adminpanel/uzytkownicy");
      }
      break;

    case 'admin':
        $id_do_edycji = $params[1];
        if($db->query('update users set admin="1" where id_user=' . $id_do_edycji))
        {
          header("Location: /barca/adminpanel/uzytkownicy");
        }
        break;

    case 'noadmin':
    case 'odblokuj':
        $id_do_edycji = $params[1];
        if($db->query('update users set admin="0" where id_user=' . $id_do_edycji))
        {
          header("Location: /barca/adminpanel/uzytkownicy");
        }
        break;

    case 'zablokuj':
        $id_do_edycji = $params[1];
        if($db->query('update users set admin="-1" where id_user=' . $id_do_edycji))
        {
          header("Location: /barca/adminpanel/uzytkownicy");
        }
        break;

    case 'haslo':
        $id_do_edycji = $params[1];
        if($db->query('update users set password="'.md5($_POST['password']).'" where id_user=' . $id_do_edycji))
        {
          header("Location: /barca/adminpanel/uzytkownicy");
        }
        break;
    case 'email':
        $id_do_edycji = $params[1];
        if($db->query('update users set email="'.$_POST['email'].'" where id_user=' . $id_do_edycji))
        {
          header("Location: /barca/adminpanel/uzytkownicy");
        }
        break;

    case 'dodaj':
        if($db->query('insert into users values(null, "'.$_POST['username'].'", "'.$_POST['email'].'", "'.md5($_POST['password']).'", 0, "default.png")'))
        {
          header("Location: /barca/adminpanel/uzytkownicy");
        }
        break;
  }



  $uzytkownicy_query = 'select * from users order by username;';
  $uzytkownicy_array = $db->query($uzytkownicy_query);
?>
<button onclick="$('.ui.form.modal.dodaj').modal('show');" class="ui green labeled icon button">
  <i class="add icon"></i>
  Dodaj nowego użytkownika
</button>

<table class="ui celled table">
  <thead>
    <tr>
      <th>Nazwa użytkownika</th>
      <th></th>
    </tr>
  </thead>
<?php
  foreach($uzytkownicy_array as $u)
  {
    switch($u['admin'])
    {
      case -1:
        $class = 'negative';
        break;
      case 1:
        $class = 'positive';
        break;
      default:
        $class = '';
        break;
    }
    if($u['id_user'] == $_SESSION['id_uzytkownika']) $class='disabled';
    echo '<tr class="'.$class.'">';
    echo '<td>' . $u['username'] . ' ('.$u['email'].')</td>';
    echo '<td class="right aligned collapsing">';
    echo '<div class="ui primary buttons">
            <div onclick="$(\'.ui.form.modal.edytuj.' . $u['id_user'] . '\').modal(\'show\');" class="ui button">Edytuj</div>
            <div class="ui floating dropdown icon button">
              <i class="dropdown icon"></i>
              <div class="menu">';
              if($u['admin'] == -1)
              {
                echo '<div onclick="$(\'.ui.basic.modal.odblokuj.' . $u['id_user'] . '\').modal(\'show\');" class="item"><i class="unlock icon"></i> Odblokuj</div>';
              }
              else
              {
                echo '<div onclick="$(\'.ui.basic.modal.zablokuj.' . $u['id_user'] . '\').modal(\'show\');" class="item"><i class="lock icon"></i> Zablokuj</div>';
              }

    echo '      <div onclick="$(\'.ui.form.modal.edytuj.' . $u['id_user'] . '\').modal(\'show\');" class="item"><i class="user icon"></i> Zmień login</div>
    <div onclick="$(\'.ui.form.modal.email.' . $u['id_user'] . '\').modal(\'show\');" class="item"><i class="at icon"></i> Zmień e-mail</div>
                <div onclick="$(\'.ui.form.modal.haslo.' . $u['id_user'] . '\').modal(\'show\');" class="item"><i class="key icon"></i> Zmień hasło</div>';
                if($u['admin'] == 0)
                {
                  echo '<div onclick="$(\'.ui.basic.modal.admin.' . $u['id_user'] . '\').modal(\'show\');" class="item"><i class="star icon"></i> Przyznaj admina</div>';
                }
                else if($u['admin'] == 1)
                {
                  echo '<div onclick="$(\'.ui.basic.modal.noadmin.' . $u['id_user'] . '\').modal(\'show\');" class="item"><i class="wheelchair icon"></i> Odbierz admina</div>';
                }

    echo '    </div>
            </div>
          </div>
          <a onclick="$(\'.ui.basic.modal.do-usuniecia.' . $u['id_user'] . '\').modal(\'show\');" class="ui red labeled icon button"><i class="trash icon"></i>usuń</a>';
    echo '<div class="ui basic modal do-usuniecia ' . $u['id_user'] . '">
            <div class="ui icon header">
              <i class="trash alternate icon"></i>
              Usunąć użytkownika "' . $u['username'] . '"?
            </div>
            <div class="content">
              <p>Operacja jest nieodwracalna.</p>
            </div>
            <div class="actions">
              <div class="ui green cancel inverted button">
                <i class="remove icon"></i>
                Nie
              </div>
              <a href="./adminpanel/uzytkownicy/usun/' . $u['id_user'] . '" class="ui red labeled icon ok button">
                <i class="trash alternate icon"></i>
                Tak
              </a>
            </div>
          </div>

          <div class="ui basic modal odblokuj ' . $u['id_user'] . '">
              <div class="ui icon header">
                <i class="unlock icon"></i>
                Odblokować użytkownika "' . $u['username'] . '"?
              </div>
              <div class="actions">
                <div class="ui secondary cancel inverted button">
                  <i class="remove icon"></i>
                  Nie
                </div>
                <a href="./adminpanel/uzytkownicy/odblokuj/' . $u['id_user'] . '" class="ui positive labeled icon ok button">
                  <i class="unlock icon"></i>
                  Tak
                </a>
              </div>
            </div>

            <div class="ui basic modal zablokuj ' . $u['id_user'] . '">
                <div class="ui icon header">
                  <i class="lock icon"></i>
                  Zablokować użytkownika "' . $u['username'] . '"?
                </div>
                <div class="actions">
                  <div class="ui secondary cancel inverted button">
                    <i class="remove icon"></i>
                    Nie
                  </div>
                  <a href="./adminpanel/uzytkownicy/zablokuj/' . $u['id_user'] . '" class="ui negative labeled icon ok button">
                    <i class="lock icon"></i>
                    Tak
                  </a>
                </div>
              </div>

              <div class="ui basic modal admin ' . $u['id_user'] . '">
                  <div class="ui icon header">
                    <i class="star icon"></i>
                    Przyznać uprawnienia administratora dla "' . $u['username'] . '"?
                  </div>
                  <div class="actions">
                    <div class="ui secondary cancel inverted button">
                      <i class="remove icon"></i>
                      Nie
                    </div>
                    <a href="./adminpanel/uzytkownicy/admin/' . $u['id_user'] . '" class="ui primary labeled icon ok button">
                      <i class="star icon"></i>
                      Tak
                    </a>
                  </div>
                </div>

                <div class="ui basic modal noadmin ' . $u['id_user'] . '">
                    <div class="ui icon header">
                      <i class="wheelchair icon"></i>
                      Odebrać uprawnienia administratora dla "' . $u['username'] . '"?
                    </div>
                    <div class="actions">
                      <div class="ui secondary cancel inverted button">
                        <i class="remove icon"></i>
                        Nie
                      </div>
                      <a href="./adminpanel/uzytkownicy/noadmin/' . $u['id_user'] . '" class="ui negative labeled icon ok button">
                        <i class="wheelchair icon"></i>
                        Tak
                      </a>
                    </div>
                  </div>


                  <form class="ui form modal edytuj ' . $u['id_user'] . '" action="./adminpanel/uzytkownicy/edytuj/' . $u['id_user'] . '" method="post">
                    <div class="header">
                      Zmień login dla "' . $u['username'] . '"
                    </div>
                    <div class="content">
                      <input type="text" name="nazwa" placeholder="Nowa nazwa..." />
                    </div>
                    <div class="actions">
                      <div class="ui black deny button">
                        Anuluj
                      </div>
                      <button type="submit" class="ui positive right labeled icon button">
                        Zapisz
                        <i class="save icon"></i>
                      </div>
                    </div>
                  </form>

                  <form class="ui form modal email ' . $u['id_user'] . '" action="./adminpanel/uzytkownicy/email/' . $u['id_user'] . '" method="post">
                    <div class="header">
                      Zmień adres e-mail dla "' . $u['username'] . '"
                    </div>
                    <div class="content">
                      <input type="text" name="email" placeholder="Nowy e-mail..." />
                    </div>
                    <div class="actions">
                      <div class="ui black deny button">
                        Anuluj
                      </div>
                      <button type="submit" class="ui positive right labeled icon button">
                        Zapisz
                        <i class="save icon"></i>
                      </div>
                    </div>
                  </form>

                  <form class="ui form modal haslo ' . $u['id_user'] . '" action="./adminpanel/uzytkownicy/haslo/' . $u['id_user'] . '" method="post">
                    <div class="header">
                      Zmień hasło dla "' . $u['username'] . '"
                    </div>
                    <div class="content">
                      <div class="ui error message"></div>
                      <div class="field">
                        <input type="password" name="password" placeholder="Nowe hasło..." />
                      </div>
                    </div>
                    <div class="actions">
                      <div class="ui black deny button">
                        Anuluj
                      </div>
                      <button type="submit" class="ui positive right labeled icon button">
                        Zapisz
                        <i class="key icon"></i>
                      </div>
                    </div>
                  </form>';

    echo '</td>';
    echo '</tr>';
  }
?>
</table>
<form class="ui form modal dodaj" action="./adminpanel/uzytkownicy/dodaj" method="post">
  <div class="header">
    Dodaj użytkownika
  </div>
  <div class="content">
    <div class="ui error message"></div>
    <div class="field">
      <input type="text" name="username" placeholder="Nazwa użytkownika" />
    </div>
    <div class="field">
      <input type="text" name="email" placeholder="Adres e-mail" />
    </div>
    <div class="field">
      <input type="password" name="password" placeholder="Hasło" />
    </div>
  </div>
  <div class="actions">
    <div class="ui black deny button">
      Anuluj
    </div>
    <button type="submit" class="ui green right labeled icon button">
      Zapisz
      <i class="save icon"></i>
    </div>
  </div>
</form>
<script>
$('.ui.form.modal.haslo')
	.form({
		fields: {
			password: {
				identifier: 'password',
				rules: [
					{
						type   : 'empty',
						prompt : 'Hasło nie może być puste'
					},
					{
						type   : 'minLength[8]',
						prompt : 'Twoje hasło musi mieć minimum 8 znaków'
					}
				]
			}
		}
	})
;
$('.ui.form.modal.dodaj')
  .form({
    fields: {
      username: {
        identifier: 'username',
        rules: [
          {
            type   : 'empty',
            prompt : 'Wpisz nazwę użytkownika'
          }
        ]
      },
      email: {
        identifier: 'email',
        rules: [
          {
            type   : 'email',
            prompt : 'Wpisz poprawny adres e-mail'
          }
        ]
      },
      password: {
        identifier: 'password',
        rules: [
          {
            type   : 'empty',
            prompt : 'Wpisz hasło'
          }
        ]
      }
    }
  });

</script>
